<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTChart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_chart', function (Blueprint $table) {
            $table->increments('id');
            $table->string('foto',100);
            $table->string('foto2',255);
            $table->string('nama_produk',30);
            $table->string('kategori',255);
            $table->integer('harga');
            $table->longtext('deskripsi',100);
            $table->string('size',5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_chart');
    }
}
