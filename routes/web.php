<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Menampilkan Semua Tampilan Admin
Route::get('/dashboard','AdminController@index')->name('dashboard');

// CRUD Banner
Route::get('/banner','BannerController@banner')->name('banner');
Route::post('/banner/proses','BannerController@proses_upload');
Route::get('/banner/hapus/{id}','BannerController@hapus');


// CRUD Produk
Route::get('/myproducts','ProdukController@produk')->name('myproducts');
Route::get('/myproducts/create','ProdukController@create');
Route::post('/myproducts','ProdukController@store');
Route::get('/myproducts/hapus/{id}','ProdukController@hapus');
Route::get('/myproducts/edit/{id}','ProdukController@edit');
Route::patch('/myproducts/{id}','ProdukController@update');


// CRUD Berita
Route::get('/berita','BeritaController@berita')->name('berita');
Route::get('/berita/create','BeritaController@create');
Route::post('/berita','BeritaController@store');
Route::get('/berita/hapus/{id}','BeritaController@hapus');
Route::get('/berita/edit/{id}','BeritaController@edit');
Route::patch('/berita/{id}','BeritaController@update');

// CRUD Category
Route::get('/mycategory','CategoryController@mycategory')->name('mycategory');
Route::get('/mycategory/create','CategoryController@create');
Route::post('/mycategory','CategoryController@store');
Route::get('/mycategory/hapus/{id}','CategoryController@hapus');
Route::get('/mycategory/edit/{id}','CategoryController@edit');
Route::patch('/mycategory/{id}','CategoryController@update');


Route::get('/inputbanner','AdminController@create');
Route::get('/user','AdminController@user')->name('user');
Route::get('/transaksi','AdminController@transaksi')->name('transaksi');
Route::get('/about','AdminController@about')->name('about');
Route::get('/inbox','AdminController@inbox')->name('inbox');
Route::get('/banner/{id}/edit','AdminController@edit');
Route::patch('/banner/{id}','AdminController@update');


//Menampilkan Semua Tampilan User
Route::get('/home','UserController@index')->name('home');
Route::get('/shop','UserController@shop')->name('shop');
Route::get('/shop/cari','UserController@cari')->name('shop.cari');
Route::get('/shop/filter','UserController@filter')->name('shop.filter');
Route::get('/blog/{id}','UserController@blog')->name('blog.id');
Route::get('/blogs','UserController@blogs')->name('blogs');
Route::get('/checkout','UserController@checkout')->name('checkout');
Route::get('/product/{id}','UserController@invoice')->name('product.{id}');
Route::get('/shop/{nama_kategori}','UserController@filterdata');
Route::get('/contact','UserController@contact')->name('contact');

//Menampilkan cart
Route::get('/cart','UserController@cart')->name('cart');
Route::post('/cart/{id}/add','ChartController@addcart');
Route::delete('/cart/{id}/hapus','ChartController@remove');
Route::get('/cart/{id}/add/session','ChartController@addsession');
Route::delete('/cart/remove/session','ChartController@removesession');
Route::post('/transaksi/add','TransaksiController@addtransaksi');



Route::get('/login','AuthController@login')->name('login');
Route::post('/login','AuthController@postLogin');
Route::get('/register','AuthController@register')->name('register');
Route::post('/register','AuthController@postRegister');
Route::get('/logout','AuthController@logout')->name('logout');
Route::get('auth/{provider}','SocialiteController@redirectToProvider');
Route::get('auth/{provider}/callback','SocialiteController@handleProviderCallback');

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
