@extends('admin.layout.admin')

@section('konten')
<!-- Form Input Banner -->
<div class="row">
  <div class="col">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Input Banner</h3>
      </div>
      <!-- /.card-header -->
      
    
      <!-- form start -->
      <form role="form" action="{{ url('banner', @$banner->id) }}" method="POST">
      	  @csrf

      	  @if(!empty($banner))
      	  		@method('PATCH')
      	  @endif
        <div class="card-body">
         <!--   <div class="form-group">
            <label for="exampleInputFile">Foto Input</label>
          <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="exampleInputFile" name="foto">
                <label class="custom-file-label" for="exampleInputFile">Pilih foto</label>
              </div>
              <div class="input-group-append">
                <span class="input-group-text" id="">Upload</span>
              </div>
            </div> 
          </div>-->
          <div class="form-group">
            <label>Input File</label>
            <input type="text" name="file" class="form-control" placeholder="Enter ..." value="{{ old('file', @$banner->file) }}">
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea class="form-control" name="keterangan" rows="3" placeholder="Enter Keterangan ..." value="{{ old('keterangan', @$banner->keterangan) }}"></textarea>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary" value="Simpan">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End Form Input Banner -->

@endsection