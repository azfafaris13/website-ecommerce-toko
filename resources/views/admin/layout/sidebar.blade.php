 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('dashboard')}}" class="brand-link">
      <img src="{{asset('pic/logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin HE Apparel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
    

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item ">
            <a href="{{url('dashboard')}}" class="nav-link {{Request::is ('dashboard') ? 'active' : '' }}">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item ">
            <a href="{{url('banner')}}" class="nav-link {{Request::is ('banner') ? 'active' : '' }}">
              <i class="nav-icon fas fa-clone"></i>
              <p>
                Banner
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('berita')}}" class="nav-link {{Request::is ('berita') ? 'active' : '' }}">
              <i class="nav-icon fas fa-newspaper"></i>
              <p>
                Berita
              </p>
            </a>
          </li>
          <li class="nav-item treeview">
            <a href="#" class="nav-link {{ Request::is('mycategory') || Request::is('myproducts') ? 'active' : '' }}">
              <i class="nav-icon fas fa-shopping-cart"></i>
              <p>
                Apparel
                <i class="right fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('mycategory')}}" class="nav-link {{ Request::is ('mycategory') ? 'active' : '' }}">
                  <i class="far fa-list-alt nav-icon"></i>
                  <p>Kategori</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('myproducts')}}" class="nav-link {{ Request::is ('myproducts') ? 'active' : '' }}">
                  <i class="far fa-bookmark nav-icon"></i>
                  <p>Produk</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{url('transaksi')}}" class="nav-link {{ Request::is ('transaksi') ? 'active' : '' }}">
              <i class="nav-icon fas fa-credit-card"></i>
              <p>
                Transaction
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('user')}}" class="nav-link {{ Request::is ('user') ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('inbox')}}" class="nav-link {{ Request::is ('inbox') ? 'active' : '' }}">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Inbox
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('about')}}" class="nav-link {{ Request::is ('about') ? 'active' : '' }}">
              <i class="nav-icon fas fa-info"></i>
              <p>
                About
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>