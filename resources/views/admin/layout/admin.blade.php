<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Admin HE Apparel</title>
  <link rel="shortcut icon" href="{{asset('pic/logo.png')}}">

  @include('admin/layout/link')

</head>
<body class="hold-transition sidebar-mini layout-fixed header-collapse sidebar-collapse control-sidebar-open">
  
  <div class="wrapper">
    @include('admin/layout/header')
    @include('admin/layout/sidebar')

    <div class="content-wrapper">
      @yield('konten')
    </div>

     @include('admin/layout/footer')
   
  </div>
 <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
      <i class="fas fa-chevron-up"></i>
    </a>
 

  <!-- REQUIRED SCRIPTS -->
  @include('admin/layout/scripts')
</body>
</html>