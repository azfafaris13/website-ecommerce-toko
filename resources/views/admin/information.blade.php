@extends('admin.layout.admin')

@section('konten')
<link rel="stylesheet" href="{{ asset('lte/http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}">

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Dashboard</a></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <!-- container-fluid -->
  <div class="container-fluid">

    <!-- START STAT BOX -->
    <div class="row">
      <!-- Best Seller -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-white">
          <div class="inner">
            <h3>{{$count_banner}}</h3>

            <p>Banner</p>
          </div>
          <div class="icon">
            <i class="fas fa-images"></i>
          </div>
          <a href="{{url('banner')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./best seller -->

      <!-- Kategori -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-white">
          <div class="inner">
            <h3>{{$count_category}}</h3>

            <p>Kategori</p>
          </div>
          <div class="icon">
            <i class="fas fa-list-alt"></i>
          </div>
          <a href="{{url('mycategory')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./kategori -->

      <!-- Produk -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-white">
          <div class="inner">
            <h3>{{$count_produk}}</h3>

            <p>Produk</p>
          </div>
          <div class="icon">
            <i class="fas fa-bookmark"></i>
          </div>
          <a href="{{url('myproducts')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./produk -->

      <!-- Berita -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-white">
          <div class="inner">
            <h3>{{$count_berita}}</h3>

            <p>Berita</p>
          </div>
          <div class="icon">
            <i class="fas fa-newspaper"></i>
          </div>
          <a href="{{url('berita')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./berita -->

      <!-- Transaksi Baru
      <div class="col-lg-3 col-6">

        <div class="small-box bg-white">
          <div class="inner">
            <h3>65</h3>

            <p>Transaksi Baru</p>
          </div>
          <div class="icon">
            <i class="fas fa-check"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
       ./transaksi baru--> 

      <!-- Transaksi Terverifikasi
      <div class="col-lg-3 col-6">
        
        <div class="small-box bg-white">
          <div class="inner">
            <h3>65</h3>

            <p>Transaksi Terverifikasi</p>
          </div>
          <div class="icon">
            <i class="fas fa-check-double"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      ./transaksi terverifikasi--> 

      <!-- Transaksi Gagal
      <div class="col-lg-3 col-6">
        <div class="small-box bg-danger">
          <div class="inner">
            <h3>65</h3>

            <p>Transaksi Gagal</p>
          </div>
          <div class="icon">
            <i class="fas fa-times"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
       ./transaksi gagal --> 

      <!-- Transaksi Sukses -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3>65</h3>

            <p>Transaksi</p>
          </div>
          <div class="icon">
            <i class="fas fa-user-check"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./transaksi sukses--> 

      <!-- Inbox -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-white">
          <div class="inner">
            <h3>65</h3>

            <p>Inbox</p>
          </div>
          <div class="icon">
            <i class="fas fa-envelope"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./transaksi -->

      <!-- Users -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-white">
          <div class="inner">
            <h3>{{$count_user}}</h3>

            <p>Users</p>
          </div>
          <div class="icon">
            <i class="fas fa-user"></i>
          </div>
          <a href="{{url('user')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./users -->
    </div>
    <!-- End STAT BOX -->


  </div>
  <!-- /.container-fluid -->
</section>
<!-- /Main content -->
    
@endsection