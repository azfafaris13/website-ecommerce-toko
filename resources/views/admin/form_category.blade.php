@extends('admin.layout.admin')

@section('konten')
<section class="content">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Input Kategori</h3>
		</div>
		<div class="card-body">

			@if(session('error'))
			<div class="alert alert-error">
				{{ sesion('error') }}
			</div>
			@endif

			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Perhatian</strong>
				<br/>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif

			<form action="{{ url('mycategory', @$category->id) }}" method="POST" enctype="multipart/form-data">
				@csrf

				@if(!empty($category))
				@method('PATCH')
				@endif
				<div class="form-group">
					<label for="exampleInputFile">File Gambar</label>
					<div class="input-group">
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="exampleInputFile" name="foto">
							<label class="custom-file-label" for="exampleInputFile">Choose file</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<b>Nama Category</b>
					<input class="form-control" name="nama_kategori" value="{{ old('nama_kategori', @$category->nama_kategori) }}"></input>
				</div>
				<input type="submit" value="Upload" class="btn btn-primary">
			</form>
		</div>
	</div>
</section>
@endsection