@extends('admin.layout.admin')

@section('konten')
<section class="content">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Input Produk</h3>
		</div>
		<div class="card-body">

			@if(session('error'))
			<div class="alert alert-error">
				{{ sesion('error') }}
			</div>
			@endif

			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Perhatian</strong>
				<br/>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif

			<form action="{{ url('myproducts', @$produk->id) }}" method="POST" enctype="multipart/form-data">
				@csrf

				@if(!empty($produk))
				@method('PATCH')
				@endif

				<div class="form-group">
					<label for="exampleInputFile">File Gambar</label>
					<div class="input-group">
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="exampleInputFile" name="foto">
							<label class="custom-file-label" for="exampleInputFile">Choose file</label>
						</div>

					</div>
				</div>
				<div class="form-group">
					<label for="exampleInputFile">File Gambar Kedua</label>
					<div class="input-group">
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="exampleInputFile" name="foto2">
							<label class="custom-file-label" for="exampleInputFile">Choose file</label>
						</div>

					</div>
				</div>
				<div class="form-group">
					<b>Nama Produk</b>
					<input class="form-control" name="nama_produk" value="{{ old('nama_produk', @$produk->nama_produk) }}"></input>
				</div>
				<div class="form-group">
					<label>Pilih Category</label>
					<select class="form-control" name="kategori">
						@foreach ($category as $row)
						<option value="{{$row->nama_kategori}}" {{ old('kategori', $row->nama_kategori) == '$row->nama_kategori' ? 'selected' : '' }}>{{$row->nama_kategori}}</option>
						@endforeach
						</select>
				</div>
				<div class="form-group">
					<b>Harga</b>
					<input class="form-control" name="harga" value="{{ old('harga', @$produk->harga) }}"></input>
				</div> 
				<div class="form-group">
					<b>Deskripsi</b>
					<textarea class="form-control" name="deskripsi">{{ old('deskripsi', @$produk->deskripsi) }}</textarea>
				</div> 
				<div class="form-group">
					<label>Pilih Size</label>
					<select class="form-control" name="size">
						<option value="S" {{ old('size', @$produk->size) == 'S' ? 'selected' : '' }}>S</option>
						<option value="M" {{ old('size', @$produk->size) == 'M' ? 'selected' : '' }}>M</option>
						<option value="L" {{ old('size', @$produk->size) == 'L' ? 'selected' : '' }}>L</option>
						<option value="XL" {{ old('size', @$produk->size) == 'XL' ? 'selected' : '' }}>XL</option>
						<option value="XXL" {{ old('size', @$produk->size) == 'XXL' ? 'selected' : '' }}>XXL</option>
					</select>
				</div>

				<input type="submit" value="Upload" class="btn btn-primary">
			</form>
		</div>
	</div>
</section>
@endsection