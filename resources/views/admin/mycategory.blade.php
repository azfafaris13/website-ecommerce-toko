@extends('admin.layout.admin')

@section('konten')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Manage Kategori</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Kategori</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<section class="content">

  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title mt-1">Data Produk</h3>
          <a href="{{ url('/mycategory/create') }}" class="ml-2">
            <i class="icon fas fa-plus-circle"></i>
          </a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          @if(session('success'))
          <div class="alert alert-success">
            {{ session('success') }}
          </div>
          @endif

          @if(session('error'))
          <div class="alert alert-error">
            {{ session('error') }}
          </div>
          @endif
          <table class="table table-bordered table-hover table-fixed" id="example2">
            <thead>
              <tr>
                <th style="width: 20px">No</th>
                <th style="width: 50px">Foto</th>
                <th>Nama Kategori</th>
                <th style="width: 20px">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($category as $row)
              <tr>
                <td>{{ isset($i) ? ++$i : $i = 1 }}</td>
                 <td><img width="150px" height="100px" src="{{ asset('data_foto_category/'.$row->foto) }}"></td>
                  <td>{{ $row->nama_kategori }}</td>
                  <td>
                    <a class="btn btn-success mb-1" style="width: 70px" href="{{ url('/mycategory/edit/'.$row->id) }}">Ubah</a>
                    <a class="btn btn-danger" style="width: 70px" href="{{ url('/mycategory/hapus/'.$row->id) }}">Hapus</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
  </section>
  @endsection