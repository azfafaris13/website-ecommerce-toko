 @extends('admin.layout.admin')

 @section('konten')
 <!-- Content Header (Page header) -->
 <div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Input Banner</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Banner</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main Content -->
<section class="content">

 <!-- START ACCORDION & CAROUSEL-->
 <div class="card">
  <div class="card-header">
    <h3 class="card-title">Carousel</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">

        @foreach($gambar->take(3) as $row)
          <div class="carousel-item @if($loop->first) active @endif">
            <img class="d-block w-100" src="{{asset ('/data_file/'.$row->file)}}" alt="{{ $row->keterangan}}" style="width: 100px;height: 600px">
          </div>
        @endforeach
        
      </div>

      <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
  <!-- /.card-body -->
</div>
<!-- END ACCORDION & CAROUSEL-->

<div class="card">
  <div class="card-header">
    <h3 class="card-title">Input Banner</h3>
  </div>
  <div class="card-body">
    <form action="{{url('/banner/proses')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}

      <div class="form-group">
        <b>File Gambar</b><br/>
        <input type="file" name="file">
      </div>

      <div class="form-group">
        <b>Keterangan Kecil</b>
        <textarea class="form-control" name="keterangan_kecil"></textarea>
      </div>
      <div class="form-group">
        <b>Keterangan</b>
        <input class="form-control" name="keterangan"></input>
      </div>

      <input type="submit" value="Upload" class="btn btn-primary">
    </form>
  </div>
</div>
<!-- /.row -->
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Data Banner</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered table-hover table-fixed" id="example2">
          <thead>
            <tr>
              <th>No</th>
              <th>ID</th>
              <th>File</th>
              <th>Keterangan Kecil</th>
              <th>Keterangan</th>
              <th style="width: 20px">Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($gambar as $row)
            <tr>
              <td>{{ isset($i) ? ++$i : $i = 1 }}</td>
              <td>{{ $row->id }}</td>
              <td>
                <img width="150px" height="100px" src="{{ url('/data_file/'.$row->file) }}"></td>
                <td>{{ $row->keterangan_kecil }}</td>
                <td>{{ $row->keterangan }}</td>
                <td>
                <!--  <form action="{{ url('/banner', $row->id) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger align-center">Delete</button>
                  </form> -->
                  <a class="btn btn-danger" href="{{ url('/banner/hapus/'.$row->id) }}">HAPUS</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
  <!-- /.row -->
</section>
<!-- /Main Content -->

@endsection