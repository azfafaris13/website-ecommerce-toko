@extends('admin.layout.admin')

@section('konten')
<section class="content">
	<div class="card">
		<div class="card-header">
			<h3 class="card-title">Input Berita</h3>
		</div>
		<div class="card-body">

			@if(session('error'))
			<div class="alert alert-error">
				{{ sesion('error') }}
			</div>
			@endif

			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Perhatian</strong>
				<br/>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif

			<form action="{{ url('berita', @$berita->id) }}" method="POST" enctype="multipart/form-data">
				@csrf

				@if(!empty($berita))
				@method('PATCH')
				@endif

				<div class="form-group">
					<label for="exampleInputFile">File Gambar</label>
					<div class="input-group">
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="exampleInputFile" name="foto">
							<label class="custom-file-label" for="exampleInputFile"></label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<b>Title</b>
					<input class="form-control" type="text" name="title" value="{{ old('title', @$berita->title) }}"></input>
				</div> 
				<div class="form-group">
						<textarea class="textarea" name="deskripsi" placeholder="Place some text here"
						style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('deskripsi', @$berita->deskripsi) }}</textarea>
				</div>

				<input type="submit" value="Upload" class="btn btn-primary">
			</form>
		</div>
	</div>
</section>
@endsection