<div> 
 <div class="cart-bg-overlay"></div>

    <div class="right-side-cart-area">

        <!-- Cart Button -->
        @if(Auth::check())
        <div class="cart-button">
            <a href="#" id="rightSideCart"><img src="{{asset('essence/img/core-img/bag.svg')}}" alt=""> <span>{{$totalcart}}</span></a>
        </div>
        @else
        @endif

        <div class="cart-content d-flex">

            <!-- Cart List Area -->
            <div class="cart-list">
                <!-- Single Cart Item -->
                @foreach($cart as $item)
                <div class="single-cart-item">
                    <a href="#" class="product-image">
                        <img src="{{$item->foto}}" class="cart-thumb" alt="">
                        <!-- Cart Item Desc -->
                        <div class="cart-item-desc">
                            <form action="{{url('/cart/'.$item->id.'/hapus')}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit">
                          <span class="product-remove"><i class="fa fa-close" aria-hidden="true"></i></span>
                          </button>
                            </form>
                            <span class="badge">{{$item->kategori}}</span>
                            <h6>{{$item->nama_produk}}</h6>
                            <p class="size">{{$item->size}}}</p>
                            <p class="color">Color: Red</p>
                            <p class="price">{{$item->harga}}</p>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>


            <!-- Cart Summary -->
            <div class="cart-amount-summary">

                <h2>Summary</h2>
                <ul class="summary-table">
                    <li><span>subtotal:</span> <span>$274.00</span></li>
                    <li><span>delivery:</span> <span>Free</span></li>
                    <li><span>discount:</span> <span>-15%</span></li>
                    <li><span>total:</span> <span>$232.00</span></li>
                </ul>
                <div class="checkout-btn mt-100">
                    <a href="checkout.html" class="btn essence-btn">check out</a>
                </div>
            </div>
        </div>
    </div>
