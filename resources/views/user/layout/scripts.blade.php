 <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="{{asset('essence/js/jquery/jquery-2.2.4.min.js') }}"></script>
    <!-- Popper js -->
    <script src="{{asset('essence/js/popper.min.js') }}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('essence/js/bootstrap.min.js') }}"></script>
    <!-- Plugins js -->
    <script src="{{asset('essence/js/plugins.js') }}"></script>
    <!-- Classy Nav js -->
    <script src="{{asset('essence/js/classy-nav.min.js') }}"></script>
    <!-- Active js -->
    <script src="{{asset('essence/js/active.js') }}"></script>
 <script>
 	$("a[href^='#']").click(function(e)
 	{
 		e.preventDefault();

 		$("body, html").animate({
 			scrollTop: $($(this).attr('href')).offset().top,
    	},
    700,'linear');
 	});
 </script>
 <script>
  $('.carousel').carousel({
    interval:3000
  });
</script>