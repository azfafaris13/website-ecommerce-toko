<header class="header_area" >
    <div class="classy-nav-container breakpoint-off d-flex align-items-center justify-content-between">
        <!-- Classy Menu -->
        <nav class="classy-navbar" id="essenceNav">
            <!-- Logo -->
            <a class="nav-brand" href="{{ url('home') }}"><img src="{{asset('pic/brand he apparel.png')}}" style="width: 150px;"></a>
            <!-- Navbar Toggler -->
            <div class="classy-navbar-toggler">
                <span class="navbarToggler"><span></span><span></span><span></span></span>
            </div>
            <!-- Menu -->
            <div class="classy-menu">
                <!-- close btn -->
                <div class="classycloseIcon">
                    <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                </div>
                <!-- Nav Start -->
                <div class="classynav">
                    <ul>
                        <li>
                            <a href="{{ url('shop') }}">Shop</a>
                            <div class="megamenu align-contents-center">
                                <ul class="single-mega cn-col-3">
                                    <li class="title">Categories</li>
                                    @foreach($header_categori->take(6) as $row)
                                    <li><a href="{{ url('/shop', $row->nama_kategori )}}">{{ $row->nama_kategori }}</a></li>
                                    @endforeach
                                </ul>
                                <ul class="single-mega cn-col-3">
                                    <li class="title">Best Apparel</li>
                                    @foreach($header_produk->take(6) as $row)
                                    <li><a href="{{url('/product/'.$row->id)}}">{{ $row->nama_produk }}</a></li>
                                    @endforeach
                                </ul>
                                <div class="single-mega cn-col-3">
                                    <div class="product_thumbnail_slides owl-carousel">
                                        @foreach($header_categori as $row)
                                        <div class="single_catagory_area d-flex align-items-center justify-content-center bg-img" style="background-image:url({{ url('/data_foto_category/'.$row->foto) }});">
                                            <div class="catagory-content">
                                                <a href="{{url('shop/'.$row->nama_kategori)}}">{{$row->nama_kategori}}</a>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li><a href="{{ url('blogs') }}">Blogs</a></li>
                        @if (Route::has('login'))
                        @auth
                        <li><a href="{{ url('cart')}}">Cart</a></li>
                        @endauth
                        @endif
                        <li><a href="{{ url('contact')}}">Contact</a></li>
                        <li>
                            
                        </li>
                    </ul>
                </div>
                <!-- Nav End -->
            </div>
        </nav>

        <!-- Header Meta Data -->
        <div class="header-meta d-flex clearfix justify-content-end">
            <!-- Search Area -->
            <div class="search-area">
                <form action="{{url('/shop/cari')}}" method="GET">
                    <input type="search" name="cari" id="headerSearch" placeholder="Search apparel" value="{{ old('cari')}}">
                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
            <!-- Favourite Area 
            <div class="favourite-area">
                <a href="#"><img src="{{asset('essence/img/core-img/heart.svg')}}" alt=""></a>
            </div> -->
            <!-- User Login Info -->
            <div class="user-login-info dropdown">
                @if (Route::has('login'))
                @auth
                <a href="{{url('shop')}}" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }}</a>
                @else
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('essence/img/core-img/user.svg')}}" alt=""></a>
                @endauth
                @endif
                @if (Route::has('login'))
                            <ul class="dropdown dropdown-menu">
                                @auth
                                    <li><a href="{{url('shop')}}">Profile</a></li>
                                    <li><a href="{{route('logout')}}">Logout</a></li>
                                    @else
                                    <li><a href="{{route('login')}}">Login</a></li>
                                    @if (Route::has('register'))
                                    <li><a href="{{route('register')}}">Register</a></li>
                                    @endif
                                @endauth
                            </ul>
                @endif
                            </div>
            <!-- Cart Area 
            <div class="cart-area">
                <a href="#" id="essenceCartBtn"><img src="{{asset('essence/img/core-img/bag.svg')}}" alt=""> <span>3</span></a>
            </div> -->
        </div>

    </div>
</header>