<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>@yield('title')</title>

    @include('user/layout/links')

</head>

<body >
   
    @yield('content')

    <!-- ##### Footer Area Start ##### -->
    @include('user/layout/footer')
    <!-- ##### Footer Area End ##### -->

   @include('user/layout/scripts')

</body>

</html>