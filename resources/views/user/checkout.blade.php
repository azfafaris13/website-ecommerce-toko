@extends('user.layout.index')

@section('title',$title)
@section('konten')
 <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb_area bg-img" style="background-image: url(essence/img/bg-img/breadcumb.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2>Checkout</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Checkout Area Start ##### -->
    <div class="checkout_area section-padding-80">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    <div class="order-details-confirmation">

                        <div class="cart-page-heading">
                            <h5>Your Order</h5>
                            <p>The Details</p>
                        </div>

                        <ul class="order-details-form mb-4">
                            <li><span>Product</span> <span>Total</span></li>
                            @foreach ( $checkout as $row)
                            <li><span style="color: #dc0345">{{$row->nama_produk}}</span> <span style="color: #dc0345">Rp {{ number_format($row->harga)}}</span></li>
                            @endforeach
                            <li><span>Subtotal</span> <span>Rp {{ number_format($subtotal)}}</span></li>
                            <li><span>Shipping</span> <span>Free</span></li>
                            <li><span>Total</span> <span>Rp {{ number_format($subtotal)}}</span></li>
                        </ul>
                        
                        <form action="{{ url('transaksi/add') }}" method="POST">
                            @csrf
                            <div class="add-to-cart-btn">
                                <button type="submit" class="btn essence-btn">Place Order</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Checkout Area End ##### -->
@endsection