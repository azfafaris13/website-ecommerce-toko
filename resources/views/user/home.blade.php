@extends('user.layout.index')

@section('title',$title)
@section('konten')
<!-- ##### Welcome Area Start ##### -->

   <!-- <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">

        @foreach($gambar->take(3) as $row)
          <div class="carousel-item @if($loop->first) active @endif">
            <img class="d-block w-100" src="{{asset ('/data_file/'.$row->file)}}" alt="{{ $row->keterangan}}" style="height: 500px">
          </div>
        @endforeach
        
      </div>

      <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
  </div> 
  <div class="single_product_thumb clearfix">
    <div class="product_thumbnail_slides owl-carousel">
        @foreach($gambar->take(3) as $row)
            <img class="d-block w-100" src="{{asset ('/data_file/'.$row->file)}}" alt="{{ $row->keterangan}}" style="height: 600px">
        @endforeach
    </div>
</div> -->
<div class="single_product_thumb clearfix">
    <div class="product_thumbnail_slides owl-carousel">
        @foreach($gambar->take(3) as $row)
        <section class="welcome_area bg-img background-overlay" style="background-image: url({{asset ('/data_file/'.$row->file)}});">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <div class="hero-content">
                            <h6 style="color: #dc0345">{{$row->keterangan_kecil}}</h6>
                            <h2>{{$row->keterangan}}</h2>
                            <a href="{{url('shop')}}" class="btn essence-btn">Belanja Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endforeach
    </div>
</div>


<!-- ##### Welcome Area End ##### -->


<!-- ##### Top Catagory Area Start ##### -->
<div class="top_catagory_area section-padding-80 clearfix">
    <div class="container">
        <div class="row justify-content-center">
            @foreach( $categori->take(3) as $row)
            <!-- Single Catagory -->
            <div class="col-12 col-sm-6 col-md-4">
                <div class="single_catagory_area d-flex align-items-center justify-content-center bg-img" style="background-image:url({{ url('/data_foto_category/'.$row->foto) }});">
                    <div class="catagory-content">
                        <a href="{{url('shop/'.$row->nama_kategori)}}">{{$row->nama_kategori}}</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- ##### Top Catagory Area End ##### -->

<!-- ##### CTA Area Start ##### -->
<div class="cta-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta-content bg-img background-overlay" style="background-image: url(essence/img/bg-img/bg-5.jpg);">
                    <div class="h-100 d-flex align-items-center justify-content-end">
                        <div class="cta--text">
                            <h6>Baca berita sedang hangat</h6>
                            <h2>Kunjungi Blog</h2>
                            <a href="{{url('blogs')}}" class="btn essence-btn">Baca Sekarang</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### CTA Area End ##### -->

<!-- ##### New Arrivals Area Start ##### -->
<section class="new_arrivals_area section-padding-80 clearfix" id="popular">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading text-center">
                    <a href="{{url('/shop')}}">
                        <h2>New Arrival</h2>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="popular-products-slides owl-carousel">

                    @foreach( $produk->take(4) as $row)

                    <!-- Single Product -->
                    <div class="single-product-wrapper">
                        <!-- Product Image -->
                        <div class="product-img">
                            <a href="{{url('product/'.$row->id)}}">
                                <img src="{{ url('/data_foto_produk/'.$row->foto) }}" style="height: 345px;width: 260px">
                                <!-- Hover Thumb -->
                                <img class="hover-img" src="{{ url('/data_foto_produk/'.$row->foto2) }}" alt="" style="height: 345px;width: 260px">
                            </a>
                                        <!-- Product Badge 
                                        <div class="product-badge offer-badge">
                                            <span>-30%</span> 
                                        </div>

                                        <!-- Product Badge -->
                                        <div class="product-badge new-badge">
                                            <span>New</span>
                                        </div>
                                        
                                        <!-- Favourite -->
                                        <div class="product-favourite">
                                            <a href="#" class="favme fa fa-heart"></a>
                                        </div>
                                    </div>

                                    <!-- Product Description -->
                                    <div class="product-description">
                                        <span>{{ $row->kategori }}</span>
                                        <a href="{{url($row->id)}}">
                                            <h6>{{ $row->nama_produk }}</h6>
                                        </a>
                                        <p class="product-price" style="color: #dc0345">Rp{{ $row->harga }}</p>

                                        <!-- Hover Content -->
                                        <div class="hover-content">
                                            <!-- Add to Cart -->
                                            <div class="add-to-cart-btn">
                                                <a href="#" class="btn essence-btn">Add to Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- ##### New Arrivals Area End ##### -->

            @endsection