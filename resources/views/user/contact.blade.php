@extends('user.layout.index')

@section('title',$title)
@section('konten')
 <div class="contact-area d-flex align-items-center">

        <div class="google-map">
            <div id="googleMap"></div>
        </div>

        <div class="contact-info">
            <h2>How to Find Us</h2>
            <p>Kami Selalu ada setiap saat,menyediakan berbagai clothes OOTD yang terupdate dan pastinya berkualitas baik</p>

            <div class="contact-address mt-50">
                <p><span>address:</span>Komp GBA 2 D4 no 20 Kab. Bandung</p>
                <p><span>telephone:</span>088222335708</p>
                <p><a href="mailto:azfafaris13@gmail.com">azfafaris13@gmail.com</a></p>
            </div>
        </div>

    </div>
@endsection