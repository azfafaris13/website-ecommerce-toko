@extends('user.layout.index')

@section('title',$title)
@section('konten')
<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb_area breadcumb-style-two bg-img" style="background-image: url({{ asset('essence/img/bg-img/breadcumb.jpg')}});">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="page-title text-center">
                    <h2 style="color: #000000">Shop</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Shop Grid Area Start ##### -->
<section class="shop_grid_area section-padding-80">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-3">
                <div class="shop_sidebar_area">

                    <!-- ##### Single Widget ##### -->
                    <div class="widget catagory mb-50">
                        <!-- Widget Title -->
                        <h6 class="widget-title mb-30">Catagories</h6>

                        <!--  Catagories  -->
                            <div class="catagories-menu">
                                <ul id="menu-content2" class="menu-content collapse show">
                                    <!-- Single Item -->
                                    <li><a href="{{ url('/shop')}}">All</a></li>
                                    @foreach($categori as $row)
                                    <li><a href="{{ url('/shop', $row->nama_kategori )}}">{{ $row->nama_kategori }}</a></span></li>
                                    @endforeach
                                  
                                </ul>
                            </div>
                    </div>

                    <!-- ##### Single Widget ##### -->


                    <!-- ##### Single Widget ##### -->

                    <!-- ##### Single Widget ##### -->

                </div>
            </div>

            <div class="col-12 col-md-8 col-lg-9">
                <div class="shop_grid_product_area">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-topbar d-flex align-items-center justify-content-between">
                                <!-- Total Products -->
                                <div class="total-products">
                                    <p><span>{{ $count_produk}}</span> apparels found</p>
                                </div>
                                <!-- Sorting -->
                                <div class="product-sorting d-flex">
                                    <p>Sort by:</p>
                                    <form action="{{'/shop'}}" method="GET">
                                        <select name="filter" id="sortByselect">
                                            <option value="highest">Highest Rated</option>
                                            <option value="newest">Newest</option>
                                            <option value="value">Price: $$ - $</option>
                                            <option value="value">Price: $ - $$</option>
                                        </select>
                                        <input type="submit" class="d-none" value="">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        @foreach( $produk as $row)

                        <!-- Single Product -->
                        <div class="col-12 col-sm-6 col-lg-4">
                            <div class="single-product-wrapper">
                                <!-- Product Image -->
                                <div class="product-img">
                                    <a href="{{url('/product/'.$row->id)}}">
                                        <img src="{{ url('/data_foto_produk/'.$row->foto) }}" style="height: 345px;width: 260px">
                                        <!-- Hover Thumb -->
                                      <img class="hover-img" src="{{ url('/data_foto_produk/'.$row->foto2) }}" alt="" style="height: 345px;width: 260px">
                                    </a>
                                        <!-- Product Badge 
                                        <div class="product-badge offer-badge">
                                            <span>-30%</span> 
                                        </div>
                                        <!-- Favourite -->
                                        <div class="product-favourite">
                                            <a href="#" class="favme fa fa-heart"></a>
                                        </div>
                                    </div>

                                    <!-- Product Description -->
                                    <div class="product-description">
                                        <span>{{ $row->kategori }}</span>
                                        <a href="{{url('/product/'.$row->id)}}">
                                            <h6>{{ $row->nama_produk }}</h6>
                                        </a>
                                        <p class="product-price" style="color: #dc0345">Rp {{ number_format($row->harga) }}</p>

                                        <!-- Hover Content -->
                                        <div class="hover-content">
                                            <!-- Add to Cart -->
                                            @if (Auth::check())
                                            <form action="{{ url('/cart/'.$row->id.'/add') }}" method="POST">
                                                @csrf
                                            <div class="add-to-cart-btn">
                                                <button type="submit" class="btn essence-btn">Add to Cart</button>
                                            </div>
                                            </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach



                        </div>
                    </div>
                    <!-- Pagination -->
                    <nav>
                   {{$produk->links()}}
                   </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Shop Grid Area End ##### -->

    @endsection