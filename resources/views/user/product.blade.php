@extends('user.layout.index')

@section('title')
{{ $produk->nama_produk}}
@endsection
@section('konten')
 <!-- ##### Single Product Details Area Start ##### -->
    <section class="single_product_details_area d-flex align-items-center">

        <!-- Single Product Thumb -->
        <div class="single_product_thumb clearfix">
            <div class="product_thumbnail_slides owl-carousel">
                <img src="{{ url('/data_foto_produk/'.$produk->foto) }}" alt="" style="width: 700px;height: 655px">
                <img src="{{ url('/data_foto_produk/'.$produk->foto2) }}" alt="" style="width: 700px;height: 655px">
            </div>
        </div>


        <!-- Single Product Description -->
        <div class="single_product_desc clearfix">
            <span>{{ $produk->kategori}}</span>
            <a href="#">
                <h2>{{ $produk->nama_produk}}</h2>
            </a>
            <p class="product-price">Rp {{ number_format($produk->harga)}}</p>
            <p class="product-desc">{{ $produk->deskripsi}}</p>
            <!-- Form -->
            <form class="cart-form clearfix" method="post">
                <!-- Select Box -->
                <div class="select-box d-flex mt-50 mb-30">
                    <select name="select" id="productSize" class="mr-5">
                        <option value="value">{{ $produk->size}}</option>
                    </select>
                </div>
                <!-- Cart & Favourite Box -->
                <div class="cart-fav-box d-flex align-items-center">
                    <!-- Cart -->
                    <form action="{{ url('cart/'.$produk->id.'/add') }}" method="POST">
                                                @csrf
                                            <div class="add-to-cart-btn">
                                                <button type="submit" class="btn essence-btn">Add to Cart</button>
                                            </div>
                                            </form>
                    <!-- Favourite -->
                    <div class="product-favourite ml-4">
                        <a href="#" class="favme fa fa-heart"></a>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!-- ##### Single Product Details Area End ##### -->

@endsection