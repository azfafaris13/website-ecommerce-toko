@extends('user.layout.index')

@section('title',$title)
@section('konten')
<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb_area breadcumb-style-two bg-img" style="background-image: url({{ asset('essence/img/bg-img/breadcumb.jpg')}});">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="page-title text-center">
                    <h2 style="color: #000000">Cart</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Shop Grid Area Start ##### -->
<section class="shop_grid_area section-padding-80">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-3">
                <div class="shop_sidebar_area">

                    <!-- ##### Single Widget ##### -->
                    

                    <!-- ##### Single Widget ##### -->


                    <!-- ##### Single Widget ##### -->

                    <!-- ##### Single Widget ##### -->

                </div>
            </div>

            
                    <div class="row">
                        <div class="col-12">
                            <div class="product-topbar d-flex align-items-center justify-content-between">
                                <!-- Total Products -->
                                <!-- Sorting -->
                            </div>
                        </div>
                    </div>

                        <table class="table">
                      <thead>
                          <tr>
                              <th scope="col">No</th>
                              <th scope="col">Kategori</th>
                              <th scope="col">Name</th>
                              <th scope="col">Price</th>
                              <th scope="col">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach( $cart as $row)
                          <tr>
                            <td>{{ isset($i) ? ++$i : $i = 1 }}</td>
                              <td>
                                  <div class="media">
                                      <div class="d-flex">  
                                        {{ $row->kategori}}   
                                      </div>
                                  </div>
                              </td>
                              <td>
                                <div class="media-body">
                                  <p>{{ $row->nama_produk}}</p>
                                </div>
                              </td>
                              <td>
                                  <h5>Rp {{ number_format($row->harga)}}</h5>
                              </td>
                              <td>
                                <form action="{{url('/cart/'.$row->id.'/hapus')}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit">
                          <span class="product-remove"><i class="fa fa-close" aria-hidden="true"></i></span>
                          </button>
                            </form>
                              </td>
                          </tr>
                          @endforeach
                          <td colspan="3">Total : </td>
                          <td colspan="2" align="center"><h5>Rp {{ number_format($subtotal) }}</h5></td>
                      </tbody>
                  </table>

          <div class="col-md-5 mb-3 mt-3">
            <a href="{{ url('checkout')}}" class="btn essence-btn">Check Out</a>
          </div>
        </div>
    </section>
    <!-- ##### Shop Grid Area End ##### -->

    @endsection