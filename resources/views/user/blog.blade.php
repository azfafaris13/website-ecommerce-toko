@extends('user.layout.index')

@section('title')
{{ $berita->title}}
@endsection
@section('konten')
<!-- ##### Blog Wrapper Area Start ##### -->

<div class="single-blog-wrapper">


<!-- Single Blog Post Thumb -->
<div class="single-blog-post-thumb">
    <div class="container-blog">
       <section class="welcome_area bg-img background-overlay" style="background-image: url({{ asset('/data_foto_berita/'.$berita->foto) }});">
            <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="hero-content">
                        <h2 style="color: #dc0345;text-shadow: 2px 2px 8px #fff;">{{ $berita->title}}</h2>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </div>
</div>

      <!--  <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-8">
                    <div class="regular-page-content-wrapper section-padding-80">
                        <div class="regular-page-text">
                            <h2>{{ $berita->title}}</h2>
                            {!! $berita->deskripsi !!}
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- Single Blog Content Wrap -->
        <div class="single-blog-content-wrapper d-flex">

            <!-- Blog Content -->
            <div class="single-blog--text">
                <h1>{{ $berita->title}}</h1>
                <h6 style="color: #787878;"><i class="fa fa-calendar"></i> {{ $berita->created_at->diffForHumans()}}</h6>
                <h6 style="color: #787878;"><i class="fa fa-eye"></i> {{ $berita->kunjungan}} Views</h6>
                <br>
                {!! $berita->deskripsi !!}
            </div>

            <!-- Related Blog Post -->
            <div class="related-blog-post">
                <!-- Single Related Blog Post -->
                <br>
                <h3>Recent Post</h3>
                <br>
                @foreach ( $related->take(4) as $row)
                <div class="single-related-blog-post" style="height: 170px;overflow: hidden;">
                    <img src="{{ url('/data_foto_berita/'.$row->foto) }}" alt="">
                    <a href="{{url('blog/'.$row->id)}}">
                        <h5>{{ $row->title}}</h5>
                    </a>
                </div>
                @endforeach
            </div>

        </div>
    </div>
    <!-- ##### Blog Wrapper Area End ##### -->

    @endsection