@extends('user.layout.auth')

@section('title',$title)
@section('content')
<!-- ##### Checkout Area Start ##### -->
<div class="checkout_area section-padding-10">
	<div class="container">
		<div class="row">

			<div class="col-12 col-md-6 ">
				<div class="single_product_thumb clearfix">
					<div class="product_thumbnail_slides owl-carousel">
						@foreach($gambar->take(3) as $row)
						<section class="welcome_area bg-img background-overlay" style="background-image: url({{asset ('/data_file/'.$row->file)}});">
							<div class="container h-100">
								<div class="row h-100 align-items-center text-center">
									<div class="col-12">
										<div class="hero-content">
											<h6 style="color: #dc0345;text-shadow: 2px 2px 8px #fff;">{{$row->keterangan_kecil}}</h6>
											<h2 style="text-shadow: 2px 2px 8px #fff;">{{$row->keterangan}}</h2>
											<a href="{{url('shop')}}" class="btn essence-btn">Belanja Sekarang</a>
										</div>
									</div>
								</div>
							</div>
						</section>
						@endforeach
					</div>
				</div>
			</div>

			<div class="col-12 col-md-6 ml-lg-auto" >
				<div class="order-details-confirmation" style="box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);">

					<div class="cart-page-heading mt-30 text-center">
						<h5>Create an Account</h5>
					</div>
					<form action="{{ route('register') }}" method="POST">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-12 mt-3">
								<label for="username">Username <span style="color: #dc0345;">*</span></label>
								<input type="text" class="form-control" id="username" value="{{ old('name')}}" name="name" required>
							</div>
							<div class="col-12 mt-3">
								<label for="email">Email <span style="color: #dc0345;">*</span></label>
								<input type="text" class="form-control" id="email" value="{{ old('email')}}" name="email" required>
							</div>
							<div class="col-12 mt-3">
								<label for="password">Password <span style="color: #dc0345;">*</span></label>
								<input type="password" class="form-control" id="password" value="" name="password" required>
							</div>
							<div class="col-12 mt-3">
								<label for="password">Confirm Password <span style="color: #dc0345;">*</span></label>
								<input type="password" class="form-control" id="password" value="" name="password_confirmation" required>
							</div>
							<div class="col-md-7 mb-2 mt-3">
								<div class="forgot_password mt-2">
								<a href="{{ url('login')}}">Have an Account ?</a>
								</div>
							</div>
							<div class="col-md-5 mt-3">
								<button class="btn essence-btn" type="submit">Register</button>
							</div>
							<div class="col-12 mt-3">
								<p class="align-items-center text-center">By registering, I agree
								Terms of Use and Privacy Policy.</p>
							</div>
							
						</div>
					</form>


				</div>
			</div>
		</div>
	</div>
</div>
<!-- ##### Checkout Area End ##### -->
@endsection