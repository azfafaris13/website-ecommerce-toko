@extends('user.layout.auth')

@section('title',$title)
@section('content')
<!-- ##### Checkout Area Start ##### -->
<div class="checkout_area section-padding-10">
	<div class="container">
		<div class="row">

			<div class="col-12 col-md-6 ">
				<div class="single_product_thumb clearfix">
					<div class="product_thumbnail_slides owl-carousel">
						@foreach($gambar->take(3) as $row)
						<section class="welcome_area bg-img background-overlay" style="background-image: url({{asset ('/data_file/'.$row->file)}});">
							<div class="container h-100">
								<div class="row h-100 align-items-center text-center">
									<div class="col-12">
										<div class="hero-content">
											<h6 style="color: #dc0345;text-shadow: 2px 2px 8px #fff;">{{$row->keterangan_kecil}}</h6>
											<h2 style="text-shadow: 2px 2px 8px #fff;">{{$row->keterangan}}</h2>
											<a href="{{url('shop')}}" class="btn essence-btn">Belanja Sekarang</a>
										</div>
									</div>
								</div>
							</div>
						</section>
						@endforeach
					</div>
				</div>
			</div>

			<div class="col-12 col-md-6 ml-lg-auto">
				<div class="order-details-confirmation" style="box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);">

					<div class="cart-page-heading mt-3 mb-3 text-center">
						<img src="{{asset('pic/brand he apparel.png')}}" alt="" style="width: 200px" class="mb-2">
						<p>He apparel wants to make all men like real men</p>
					</div>
					<form action="{{ route('login')}}" method="POST">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-12 mb-3 mt-30">
								<label for="email">Email</label>
								<input type="email" class="form-control" id="email" value="" name="email">
							</div>
							<div class="col-12 mb-5 mt-3">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="password" value="" name="password">
							</div>
							<div class="col-md-7 mb-2 mt-3">
								<a href="{{url('register')}}" class="btn essence-btn">Register</a>
								<div class="forgot_password mt-3">
								<a href="#">Forgot Password?</a>
								</div>
								<br>
							</div>
							<div class="col-md-5 mb-3 mt-3">
								<button class="btn essence-btn" type="submit">Login</button>
							</div>
							<!--<div class="col-12">
								<a href="{{ url('/auth/facebook') }}" class="btn essence-btn">
									<i class="fas fa-facebook"></i>
									Login with Facebook
								</a>
							</div>
							-->
						</div>
					</form>


				</div>
			</div>
		</div>
	</div>
</div>
<!-- ##### Checkout Area End ##### -->
@endsection