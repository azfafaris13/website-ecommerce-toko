@extends('user.layout.index')

@section('title', $title)
@section('konten')
 <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb_area breadcumb-style-two bg-img" style="background-image: url({{asset('essence/img/bg-img/breadcumb2.jpg')}});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="page-title text-center">
                        <h2 style="color: #000000">blogs</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Blog Wrapper Area Start ##### -->
    <div class="blog-wrapper section-padding-80">
        <div class="container">
            <div class="row">
               
               @foreach( $berita as $row)
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-6">
                    <div class="single-blog-area mb-50" style="height: 250px;overflow: hidden;">
                        <a href="{{url('blog/'.$row->id)}}">
                        <img src="{{ url('/data_foto_berita/'.$row->foto) }}" alt="">
                        <!-- Post Title -->
                        <div class="post-title">
                            <h6 style="color: #787878;"><i class="fa fa-calendar"></i> {{ $row->created_at->format('d M Y')}}</h6>
                            <h6 style="color: #787878;"><i class="fa fa-eye"></i> {{ $row->kunjungan}} Views</h6>
                            <a href="{{url('blog/'.$row->id)}}">{{ $row->title }}</a>
                        </div>
                        <!-- Hover Content -->
                        <div class="hover-content">
                            <!-- Post Title -->
                            <div class="hover-post-title">
                                <a href="{{url('blog/'.$row->id)}}">{{ $row->title }}</a>
                            </div>
                            <p>{!! str_limit($row->deskripsi, 100, ' ...') !!}</p>
                            <a href="{{url('blog/'.$row->id)}}">Continue reading <i class="fa fa-angle-right"></i></a>
                        </div>
                        </a>
                     
                    </div>
                </div>
                @endforeach
               
               
               
                
            </div>
        </div>
    </div>
    <!-- ##### Blog Wrapper Area End ##### -->
@endsection