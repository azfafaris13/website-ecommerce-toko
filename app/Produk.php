<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = "t_produk";

    protected $fillable = ['foto','foto2','nama_produk','kategori','harga','deskripsi','size','kunjungan'];
}
