<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
    protected $table = "t_banner";

    protected $fillable = ['file','keterangan_kecil','keterangan'];
}