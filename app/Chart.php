<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chart extends Model
{
    protected $table = "t_chart";

    protected $fillable = ['pemilik','id_produk','foto','nama_produk','kategori','harga','size'];
}
