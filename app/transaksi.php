<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
   protected $table = "t_transaksi";

   protected $fillable = "pemilik,nama_produk,kategori,harga,deskripsi";


}
