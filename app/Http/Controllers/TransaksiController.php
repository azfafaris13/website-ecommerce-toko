<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Chart;
use App\Transaksi;

class TransaksiController extends Controller
{
   public function addtransaksi(Request $request,$pemilik){

    	$cart = Chart::find($pemilik);
    	$transaksi = Transaksi::where('pemilik',$cart->pemilik)->first();
    	
    	if ($transaksi) {
    	 	return redirect()->back()->with('success','Produk ini telah ditambahkan');
    	 } else {
    		Chart::create([
    			'pemilik' => $cart->pemilik,	
				'nama_produk' => $produk->nama_produk,
				'kategori' => $produk->kategori,
				'harga' => $produk->harga,
				 ]);	
    	}

    	return redirect('user/home')->with('success','Produk ini telah ditambahkan');
    	
    }

     public function remove($id){

    	Transaksi::where('id',$id)->delete();
    	return redirect()->back()->with('success','Produk ini telah dihapus');
    }
}
