<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Chart;
use App\Produk;

class ChartController extends Controller
{
    public function addcart(Request $request, $id){

    	$produk = Produk::find($id);
    	$cart = Chart::where('pemilik',auth()->user()->name)->where('id',$produk->id)->first();
    	
    	if ($cart) {
    	 	return redirect()->back()->with('success','Produk ini telah ditambahkan');
    	 } else {
    		Chart::create([
    			'pemilik' => auth()->user()->name,	
				'id_produk' => $produk->id,	
				'foto' => $produk->foto,
				'nama_produk' => $produk->nama_produk,
				'kategori' => $produk->kategori,
				'harga' => $produk->harga,
				'size' => $produk->size,
				 ]);	
    	}

    	return redirect()->back()->with('success','Produk ini telah ditambahkan');
    	
    }

    public function remove($id){

    	Chart::where('id',$id)->delete();
    	return redirect()->back()->with('success','Produk ini telah dihapus');
    }

}
