<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gambar;
use App\Produk;
use App\Category;
use App\Berita;
use App\User;
use File;

class AdminController extends Controller
{
    // Menampilkan Konten Dashboard
    public function index(){

        $count_banner = Gambar::count();
        $count_produk = Produk::count();
        $count_category = Category::count();
        $count_berita = Berita::count();
        $count_user = User::count();
        return view('admin/information',compact('count_banner','count_produk','count_category','count_berita','count_user'));
    }

  
    // Menampilkan Konten Inbox
    public function inbox(){
    	return view('admin/inbox');
    }

    // Menampilkan Konten Transaksi
    public function transaksi(){
    	return view('admin/transaksi');
    }

    // Menampilkan Konten User
    public function user(){

        $user = User::get();
    	return view('admin/user',compact('user'));
    }

    // Menampilkan Konten About
    public function about(){
        return view('admin/about');
    }

    

}
