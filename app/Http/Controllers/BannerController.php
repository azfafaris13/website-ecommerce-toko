<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gambar;
use File;

class BannerController extends Controller
{
   public function banner(){
        $gambar = Gambar::get();
        return view('admin/banner',['gambar' => $gambar]);
    }
    public function proses_upload(Request $request){
        $this->validate($request, [
            'file' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'keterangan_kecil' => 'required',
            'keterangan' => 'required',
        ]);
        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('file');
        $nama_file = time()."_".$file->getClientOriginalName();
                 // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_file';
        $file->move($tujuan_upload,$nama_file);
        Gambar::create([
            'file' =>$nama_file,
            'keterangan_kecil' =>$request->keterangan_kecil,
            'keterangan' =>$request->keterangan,
        ]);
        return redirect()->back();
    }
    public function hapus($id){
    // hapus file
        $gambar = Gambar::where('id',$id)->first();
        File::delete('data_file/'.$gambar->file);
    // hapus data
        Gambar::where('id',$id)->delete();
        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $gambar = Gambar::get()->find($id);
        return view('admin/banner',['gambar' => $gambar]);
    }
}
