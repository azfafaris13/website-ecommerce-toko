<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Category;
use File;

class ProdukController extends Controller
{
	public function produk(){
        $data['produk'] = \DB::table('t_produk')->get();
        return view('admin/myproducts', $data);
    }
    public function create(){
   
    		$category = Category::get();
            return view('admin/form_produk',compact('category'));
    }
    public function store(Request $request)
    {
         $rule = [
            'foto' => 'required|file|image|mimes:svg,jpeg,png,jpg|max:2048',
            'foto2' => 'required|file|image|mimes:svg,jpeg,png,jpg|max:2048',
            'nama_produk' => 'required|string',
            'kategori' => 'required|string',
            'harga' => 'required|numeric',
            'deskripsi' => 'required|string',
            'size' => 'required',
        ];
        $this->validate($request, $rule);

       // menyimpan data file yang diupload ke variabel $file
       $file = $request->file('foto','foto2');
        $file2 = $request->file('foto2');

        $nama_foto = time()."_".$file->getClientOriginalName();
        $nama_foto_kedua = time()."_".$file2->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_foto_produk';
        $file->move($tujuan_upload,$nama_foto);
        $file2->move($tujuan_upload,$nama_foto_kedua);

        $status = Produk::create([
                'foto' =>$nama_foto,
                'foto2' =>$nama_foto_kedua,
                'nama_produk' =>$request->nama_produk,
                'kategori' =>$request->kategori,
                'harga' =>$request->harga,
                'deskripsi' =>$request->deskripsi,
                'size' =>$request->size,
            ]);

        if ($status) {
            return redirect('/myproducts')->with('success','Data Berhasil ditambahkan');
        } else {
            return redirect('/myproducts/create')->with('error','Data Gagal ditambahkan');
        }
        
    }
    public function hapus(Request $request, $id){
   // hapus file
        $produk = Produk::where('id',$id)->first();
        File::delete('data_foto_produk/'.$produk->file);
    // hapus data
       $status =  Produk::where('id',$id)->delete();
        if ($status) {
            return redirect('/myproducts')->with('success','Data Berhasil dihapus');
        } else {
            return redirect('/myproducts/create')->with('error','Data Gagal dihapus');
        }
    }
    public function edit(Request $request, $id)
    {
        $data['produk'] = \DB::table('t_produk')->find($id);
        $data['category'] = \DB::table('t_kategori')->get();
        return view('admin/form_produk',$data);
    }
    public function update(Request $request, $id)
    {
         $rule = [
            'foto' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'foto2' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'nama_produk' => 'required|string',
            'kategori' => 'required|string',
            'harga' => 'required|numeric',
            'deskripsi' => 'required|string',
            'size' => 'required',
        ];
        $this->validate($request, $rule);

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('foto','foto2');
        $file2 = $request->file('foto2');

        $nama_foto = time()."_".$file->getClientOriginalName();
        $nama_foto_kedua = time()."_".$file2->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_foto_produk';
        $file->move($tujuan_upload,$nama_foto);
        $file2->move($tujuan_upload,$nama_foto_kedua);


        $status = Produk::where('id',$id)->update([
                'foto' =>$nama_foto,
                'foto2' =>$nama_foto_kedua,
                'nama_produk' =>$request->nama_produk,
                'kategori' =>$request->kategori,
                'harga' =>$request->harga,
                'deskripsi' =>$request->deskripsi,
                'size' =>$request->size,
            ]);

      if ($status) {
            return redirect('/myproducts')->with('success','Data Berhasil dirubah');
        } else {
            return redirect('/myproducts/create')->with('error','Data Gagal dirubah');
        }
    }

    public function filterdata(Request $request)
    {   
        $kategoriproduk = $request->nama_kategori;
        $data['produk'] = \DB::table('t_produk')->where('kategori','=',$kategoriproduk)->get();
        $data['kategori'] = \DB::table('t_kategori')->get();
        return view('user/shop',$data);
    }

    
}
