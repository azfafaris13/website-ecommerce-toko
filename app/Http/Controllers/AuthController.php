<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gambar;
use App\Produk;
use App\Berita;
use App\Category;
use App\User;
use Auth;


class AuthController extends Controller
{
    public function login(){
        $title = 'Login';
        $gambar = Gambar::get();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
      
        return view('user/login',compact('title','gambar','header_produk','header_categori'));
    }
    public function postLogin(Request $request){
        $gambar = Gambar::get();
        $produk = Produk::orderByDesc('created_at','desc')->get();
        $categori = Category::orderByDesc('created_at')->get();
        $berita = Berita::orderByDesc('updated_at')->get();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
        
        if (Auth::attempt(['email'=>$request->email,'password'=>$request->password])) {
            return redirect()->route('home',compact('gambar','produk','categori','berita','header_categori','header_produk'));
        	
        }

      return redirect()->back();
        

    }
    public function register(){
        $title = 'Register';
        $gambar = Gambar::get();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
      
        return view('user/register',compact('title','gambar','header_produk','header_categori'));
    }
    public function postRegister(Request $request){
        $gambar = Gambar::get();
        $produk = Produk::orderByDesc('created_at','desc')->get();
        $categori = Category::orderByDesc('created_at')->get();
        $berita = Berita::orderByDesc('updated_at')->get();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();

        $this->validate($request,[
        	'name' => 'required|min:4',
        	'email' => 'required|email|unique:t_users',
        	'password' => 'required|min:8|confirmed'
        ]);

        User::create([
        	'name' => $request->name,
        	'email' => $request->email,
        	'password' => bcrypt($request->password)
        ]);
      
         return redirect()->route('login',compact('gambar','produk','categori','berita','header_categori','header_produk'));
    }

    public function logout(){
    	Auth::logout();

    	return redirect()->route('login');
    }
}
