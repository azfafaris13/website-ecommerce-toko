<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gambar;
use App\Produk;
use App\Berita;
use App\Category;
use App\Chart;
use App\Transaksi;
use Auth;

class UserController extends Controller
{
	// Menampilkan konten home
    public function index(){

        $title = 'HE Apparel';
        $gambar = Gambar::get();
        $produk = Produk::orderByDesc('created_at','desc')->get();
        $categori = Category::orderByDesc('created_at')->get();
        $berita = Berita::orderByDesc('updated_at')->get();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
        return view('user/home',compact('title','gambar','produk','categori','berita','header_categori','header_produk'));
    }

    public function contact(){

        $title = 'Contact Us';
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
        return view('user/contact',compact('title','header_categori','header_produk'));
    }

    // Menampilkan konten product
    public function invoice($id){

        Produk::where('id',$id)->increment('kunjungan');
        $title = 'Blogs';
        $produk = Produk::find($id);
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
        return view('user/product',compact('title','produk','header_produk','header_categori'));
    }
    // Menampilkan konten shop
    public function shop(Request $request){

      //  $filter = $request->get('filter');

    if (Auth::check()) {
        $title = 'Shop';
        $produk = Produk::orderBy('kategori')->paginate(9);
        $categori = \DB::table('t_kategori')->get();
        $count_produk = \DB::table('t_produk')->count();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
        $cart = Chart::orderBy('created_at','desc')->where('pemilik',auth()->user()->name)->get();
        $totalcart = Chart::where('pemilik',auth()->user()->name)->count();

        return view('user/shop',compact('title','produk','categori','count_produk','header_produk','header_categori','cart','totalcart'));      
    }else{
        $title = 'Shop';
        $produk = Produk::orderBy('kategori')->paginate(9);
        $categori = \DB::table('t_kategori')->get();
        $count_produk = \DB::table('t_produk')->count();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
        $cart = Chart::orderBy('created_at','desc')->get();
        $totalcart = Chart::count();
        
        return view('user/shop',compact('title','produk','categori','count_produk','header_produk','header_categori','cart','totalcart'));
       
    }
 
    }
    public function cari(Request $request){

        $cari = $request->get('cari');

        $title = $request->get('cari');
        $produk = Produk::where('nama_produk','like',"%".$cari."%")
        ->orwhere('kategori','like',"%".$cari."%")->paginate(9);
        $categori = Category::paginate(9);
        $count_produk = Produk::where('nama_produk','like',"%".$cari."%")
        ->orwhere('kategori','like',"%".$cari."%")->count(); 
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
      
        return view('user/shop',compact('title','produk','categori','count_produk','header_produk','header_categori'));
    }
    
    public function filterdata(Request $request){

        $title = 'Shop';
        $category = $request->nama_kategori;
        $produk = \DB::table('t_produk')->where('kategori', '=' ,$category)->paginate(9);
        $categori = \DB::table('t_kategori')->paginate(9);
        $count_produk = \DB::table('t_produk')->where('kategori', '=' ,$category)->count();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();

        return view('user/shop',compact('title','produk','categori','count_produk','header_produk','header_categori'));
    }
    // Menampilkan konten blog
    public function blog($id){
        Berita::where('id',$id)->increment('kunjungan');
        $title = 'Blog';
        $berita = Berita::find($id);
        $related = Berita::orderByDesc('created_at')->get();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
      
        return view('user/blog',compact('title','berita','related','header_produk','header_categori'));
    }
    // Menampilkan konten blogs
    public function blogs(){
        $title = 'Blogs';
        $berita = Berita::orderBy('created_at','desc')->get();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
      
        return view('user/blogs',compact('title','berita','header_produk','header_categori'));
    }
    // Menampilkan konten cart
    public function cart(){
        if (Auth::check()) {
        $title = 'Cart';
        $subtotal = Chart::where('pemilik',auth()->user()->name)->sum('harga');
        $cart = Chart::orderBy('created_at','desc')->where('pemilik',auth()->user()->name)->get();
        $totalcart = Chart::where('pemilik',auth()->user()->name)->count();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();

        return view('user/chart',compact('title','subtotal','cart','totalcart','header_produk','header_categori'));
        }
        else{
        $title = 'Cart';
        $subtotal = Chart::where('pemilik',auth()->user()->name)->sum('harga'); 
        $cart = Chart::orderBy('created_at','desc')->get();
        $totalcart = Chart::count();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
        return view('user/chart',compact('title','subtotal','cart','totalcart','header_produk','header_categori'));
        }
        
    }
    // Menampilkan konten checkout
    public function checkout(){

        if(Auth::check()){
        $title = 'Checkout';
        $subtotal = Chart::where('pemilik',auth()->user()->name)->sum('harga');
        $checkout = Chart::orderBy('created_at','desc')->where('pemilik',auth()->user()->name)->get();
        $totalcheckout = Chart::where('pemilik',auth()->user()->name)->count();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();

        return view('user/checkout',compact('title','subtotal','checkout','totalcheckout','header_produk','header_categori'));
        }
        else{
        $title = 'Checkout';
        $subtotal = Chart::where('pemilik',auth()->user()->name)->sum('harga'); 
        $checkout = Chart::orderBy('created_at','desc')->get();
        $totalcheckout = Chart::count();
        $header_produk = Produk::orderByDesc('kunjungan')->get();
        $header_categori = Category::orderByDesc('created_at')->get();
        return view('user/checkout',compact('title','subtotal','checkout','totalcheckout','header_produk','header_categori'));
        }
    }

}
