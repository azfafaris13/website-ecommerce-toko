<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use File;

class BeritaController extends Controller
{
    public function berita(){
        $data['berita'] = \DB::table('t_berita')->get();
        return view('admin/berita', $data);
    }
    public function create(){
   
            return view('admin/form_berita');
    }
    public function store(Request $request)
    {
         $rule = [
            'foto' => 'required|file|image|mimes:svg,jpeg,png,jpg|max:2048',
            'title' => 'required|string',
            'deskripsi' => 'required',
            ];
        $this->validate($request, $rule);

       // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('foto');

        $nama_foto = time()."_".$file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_foto_berita';
        $file->move($tujuan_upload,$nama_foto);


        $status = Berita::create([
                'foto' =>$nama_foto,
                'title' =>$request->title,
                'deskripsi' =>$request->deskripsi,
               
            ]);

        if ($status) {
            return redirect('/berita')->with('success','Data Berhasil ditambahkan');
        } else {
            return redirect('/berita/create')->with('error','Data Gagal ditambahkan');
        }
        
    }
    public function hapus(Request $request, $id){
   // hapus file
        $berita = Berita::where('id',$id)->first();
        File::delete('data_foto_berita/'.$berita->file);
    // hapus data
       $status =  Berita::where('id',$id)->delete();
        if ($status) {
            return redirect('/berita')->with('success','Data Berhasil dihapus');
        } else {
            return redirect('/berita/create')->with('error','Data Gagal dihapus');
        }
    }
    public function edit(Request $request, $id)
    {
        $data['berita'] = \DB::table('t_berita')->find($id);
       	return view('admin/form_berita',$data);
    }
    public function update(Request $request, $id)
    {
         $rule = [
            'foto' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'title' => 'required|string',
           
            ];
        $this->validate($request, $rule);

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('foto');

        $nama_foto = time()."_".$file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_foto_berita';
        $file->move($tujuan_upload,$nama_foto);


        $status = Berita::where('id',$id)->update([
                'foto' =>$nama_foto,
                'title' =>$request->title,
                'deskripsi' =>$request->deskripsi,
               
            ]);

      if ($status) {
            return redirect('/berita')->with('success','Data Berhasil dirubah');
        } else {
            return redirect('/berita/create')->with('error','Data Gagal dirubah');
        }
    }
}
	