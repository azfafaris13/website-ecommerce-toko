<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function mycategory(){

    	$category = Category::get();
    	return view('admin/mycategory',compact('category'));
    }

    public function create(){
   
              return view('admin/form_category');
    }
    public function store(Request $request)
    {
         $rule = [
            'foto' => 'required|file|image|mimes:svg,jpeg,png,jpg|max:2048',
            'nama_kategori' => 'required|string',
        ];
        $this->validate($request, $rule);
        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('foto');

        $nama_foto = time()."_".$file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_foto_category';
        $file->move($tujuan_upload,$nama_foto);

        $status = Category::create([
                'foto' =>$nama_foto,
                'nama_kategori' =>$request->nama_kategori,
                ]);

        if ($status) {
            return redirect('/mycategory')->with('success','Data Berhasil ditambahkan');
        } else {
            return redirect('/mycategory/create')->with('error','Data Gagal ditambahkan');
        }
        
    }
    public function hapus(Request $request, $id){
   // hapus file
        $category = Category::where('id',$id)->first();
         File::delete('data_foto_category/'.$category->file);
      // hapus data
       $status =  Category::where('id',$id)->delete();
        if ($status) {
            return redirect('/mycategory')->with('success','Data Berhasil dihapus');
        } else {
            return redirect('/mycategory/create')->with('error','Data Gagal dihapus');
        }
    }
    public function edit(Request $request, $id)
    {
        $data['category'] = \DB::table('t_kategori')->find($id);
        return view('admin/form_category',$data);
    }
    public function update(Request $request, $id)
    {
        $rule = [
            'foto' => 'required|file|image|mimes:svg,jpeg,png,jpg|max:2048',
            'nama_kategori' => 'required|string',
        ];
        $this->validate($request, $rule);
        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('foto');

        $nama_foto = time()."_".$file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_foto_category';
        $file->move($tujuan_upload,$nama_foto);

        $status = Category::where('id',$id)->update([
                'foto' =>$nama_foto,
                'nama_kategori' =>$request->nama_kategori,
                ]);

      if ($status) {
            return redirect('/mycategory')->with('success','Data Berhasil dirubah');
        } else {
            return redirect('/mycategory/create')->with('error','Data Gagal dirubah');
        }
    }

}
