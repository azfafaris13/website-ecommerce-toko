<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "t_kategori";

    protected $fillable = ['foto','nama_kategori'];
}
