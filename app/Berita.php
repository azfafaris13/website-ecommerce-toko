<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = "t_berita";

    protected $fillable = ['foto','title','deskripsi','kunjungan','updated_at'];
}
